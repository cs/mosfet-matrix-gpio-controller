# MOSFET Matrix GPIO Controller
This program controls the MOSFET driver in the MOSFET matrix box,
switching power supplies to different coils.

## Installation
This program uses the standard Linux GPIO interface to control the output
pins. The default setup of the MOSFET box uses two Adafruit GPIO Expander
Bonnet stacked on top of a Raspberry Pi. They use the MCP23017 to provide
16 GPIOs each.

To use two expanders with the Pi it is necessary to assign a different I2C
address to one of them, otherwise they will both listen on their standard
address `0x20`. This is done by closing a solder bridge on the expander
board. It is recommended to close `A0` on one of them, this increases the
address by 1 to `0x21`.

Then to tell the Linux kernel to use those edit the `/boot/config.txt` of
the Raspberry Pi and add the two following lines. If less/more boards and
other addresses are used adjust the entries accordingly.
```
dtoverlay=mcp23017,addr=0x20,noints=1
dtoverlay=mcp23017,addr=0x21,noints=1
```
After a reboot the expanders should be usable. To check this install
`gpiod` and the python bindings using `apt install gpiod
python3-libgpiod`. Now you can run `gpiodetect` and should get something
like this:
```shell
pi@raspberrypi:~ $ gpiodetect
gpiochip0 [pinctrl-bcm2835] (54 lines)
gpiochip1 [brcmvirt-gpio] (2 lines)
gpiochip2 [raspberrypi-exp-gpio] (8 lines)
gpiochip3 [mcp23017] (16 lines)
gpiochip4 [mcp23017] (16 lines)
```
The last two lines on the bottom are the expander boards. Sadly there is
currently no way to tell which one is which. To figure this out set one
output high and measure the voltage with the multimeter. The following
command will set pin 0 on chip 3 to 1, i.e. 5V.
```shell
pi@raspberrypi:~ $ gpioset 3 0=1
```
In our case `gpiochip3` has address `0x21` and `gpiochip4` has address
`0x20`.
