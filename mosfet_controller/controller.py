from typing import Dict, Optional

from mosfet_controller.gpio import Supply


class MOSFETController:
    def __init__(self, supplies: Dict[str, Supply]):
        self._supplies = supplies

    def status(self) -> Dict[str, int]:
        return {name: supply.output for name, supply in self._supplies.items()}

    def get_supply(self, supply: str):
        return self._supplies[supply].output

    def set_supply(self, supply: str, coil: Optional[int]):
        self._supplies[supply].output = coil
