from typing import Optional

from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

from mosfet_controller.controller import MOSFETController
from mosfet_controller.gpio import Supply

supply_config = {
    3: {"TOP2": range(7, 0, -1), "TOP1": range(15, 8, -1)},
    4: {"BOT2": range(7, 0, -1), "BOT1": range(15, 8, -1)},
}


app = FastAPI()
supplies = {}
for gpio_number, config in supply_config.items():
    supplies.update(Supply.init_chip(gpio_number, config))
controller = MOSFETController(supplies)


@app.get("/status", response_model=dict[str, Optional[int]])
async def status():
    return controller.status()


@app.get("/supplies", response_model=list[str])
async def supplies():
    return list(controller.status().keys())


class SupplyModel(BaseModel):
    name: str
    output: Optional[int]


@app.get(
    "/supply/{name}",
    response_model=SupplyModel,
    responses={404: {"description": "supply not found"}},
)
async def get_supply(name: str):
    try:
        return SupplyModel(name=name, output=controller.get_supply(name))
    except KeyError:
        raise HTTPException(status_code=404, detail="supply not found")


@app.post(
    "/supply/{name}",
    response_model=SupplyModel,
    responses={404: {"description": "supply not found"}},
)
async def set_supply(name: str, output: int):
    try:
        if output == 0:
            output = None
        controller.set_supply(name, output)
        return SupplyModel(name=name, output=controller.get_supply(name))
    except KeyError:
        raise HTTPException(status_code=404, detail="supply not found")


def run_uvicorn():
    import uvicorn

    uvicorn.run(app, host="0.0.0.0")
