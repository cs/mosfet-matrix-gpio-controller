from mosfet_controller.api import run_uvicorn

if __name__ == "__main__":
    run_uvicorn()
