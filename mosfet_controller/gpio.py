from __future__ import annotations

from typing import Dict, Iterable, List, Mapping, Optional

import gpiod


class Supply:
    """gpio control for one power supply"""

    def __init__(self, name: str, gpio, pins: List[int]):
        self._current: Optional[int] = None
        self._no_outputs = len(pins)

        request_config = gpiod.line_request()
        request_config.consumer = name
        request_config.request_type = request_config.DIRECTION_OUTPUT

        self._lines = gpio.get_lines(pins)
        self._lines.request(request_config)
        self._lines.set_values([0] * self._no_outputs)

    @classmethod
    def init_chip(
        cls, gpio_number: int, config: Mapping[str, Iterable[int]]
    ) -> Dict[str, Supply]:
        gpio = gpiod.chip(str(gpio_number))
        return {name: cls(name, gpio, list(pins)) for name, pins in config.items()}

    @property
    def output(self) -> Optional[int]:
        return self._current

    @output.setter
    def output(self, coil: Optional[int]):
        if coil is None:
            self.disable()
            return

        if coil < 1 or coil > 7:
            raise TypeError("coil must be between 1 and 7 (inclusive)")

        values = [0] * self._no_outputs
        values[coil - 1] = 1
        self._lines.set_values(values)
        self._current = coil

    def disable(self):
        self._lines.set_values([0] * self._no_outputs)
        self._current = None
